//
let x = 5
const num = x**3;
let message = (`The cube of ` + x + ` is ` + num);
console.log(message);
//
let address = ["258", "Washington Ave NW", "California", "90011"];

let [number, avenue, state, zip] = address;
let message2 = `I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`;
console.log(message2);
//
let animal = {
	Name: "Lolong",
	Type: "saltwater crocodile",
	Weight: "1075 kgs",
	Measurement: "20 ft 3 in."
}
const {Name, Type, Weight, Measurement} = animal;

function getDetails ({Name, Type, Weight, Measurement}){
	console.log(`${Name} was a ${Type}. He weighed at ${Weight} with a measurement of ${Measurement}`);
	}

getDetails(animal);
//
const arrayNum = [1,2,3,4,5];
arrayNum.forEach( (arrayNum) => console.log(arrayNum));

var reduceNumber = (prev,curr) => prev + curr;
console.log(arrayNum.reduce(reduceNumber));
//
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = "5";
myDog.breed = "Miniature Dachshund";

console.log(myDog);
